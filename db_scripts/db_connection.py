import pymysql


def get_connection():
    try:
        db = pymysql.connect(host='localhost',
                            user='root',
                            password='neueda',
                            db='spark',
                            charset='utf8',
                            cursorclass=pymysql.cursors.DictCursor)
    except Exception as e:
        logger.exception("Failed to connect to MySQL at <{0!s}>: <{1!r}>")
        raise e
    return db
